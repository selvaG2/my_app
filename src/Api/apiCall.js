import axios from "axios";

export default axios.create({
    baseURL:"https://my-app-json-server.onrender.com/"
})